# Work log

## Executing the code

1. Decided to use make to make it reasonably universal. The funny thing is that now I'm forced to use a Windows machine, and setting make up on Windows isn't that trivial. What do you know.
2. I also wanted to go with an environment variable for base paths... TODO check how to do this in a way that is compatible for windows and Unix systems.
3. Decided to manually set Env Vars.

## Work log for RNA-Seq Section

### What is it about?

1. Data contains ~2.1k samples, 20k cols
    - Each column expression counts (per million) of an specific gene.
    - Given that there are 3 major groups, inside which there may be many other subgroups. Subgroups of a given group can manifest similar functionality. Knowing that functionality and RNA expression are related, we can assume that similar expression numbers will be observed for cells of a given class.
2. Does it contain null values?
    - No. "No expression" is represented with zeroes.
    - From the [protocol specification](https://portal.brain-map.org/atlases-and-data/rnaseq/protocols-mouse-cortex-and-hippocampus) there will also be no entirely empty columns. "RNAseq ..." section explains that cells are required to express a minimum of 1000 genes with CPM > 0.
    - From this section, it's also noted that " Cells that met the following criteria were filtered out for downstream processing: neurons with fewer than 2000 detected genes and non-neuronal cells with fewer than 1000 detected genes. "
3. Quick descriptives
    - Taking a quick look at a Describe run, we can see average values vary quite widely, but also that within most columns there are mostly 0s and a few high-valued examples.
    - We also have a wide array of standard deviations (hence variances), which means that dimensionality reduction should also probably use whitening?

### How to visualize cell groups

1. Considering that there are three main groups with many subgroups and that these should be relatively similar in Gene Expression metrics, we can maybe attempt to group cells via agglomerative clustering?
    - Or maybe group genes using agglomerative clustering and later run a PCA on each group of genes to select a few that summarize each?
    - A good-enough baseline is to just scale all features with log1p (as suggested) -> PCA -> Agglomerative Clustering
        - Seems OK, yielded one clearly separated cluster and two intermigled ones (setting n_clusters to 3 as previously determined). Used cosine as all features are numeric and I expected alignment in some dimensions (expressions of set of genes) to be interpretable as similarity.
        - Attempt with L1 distance yielded basically a single cluster
        - Next step should probably be to attempt to recluster this with an un-specified number of clusters
2. As a next step from the baseline above (which in retrospect I should probably keep on the final version), performing a search on silhouette, davies-bouldin;

### Classifying Cells

1. Target:
    - Column '0' represents cell classification.
    - Baseline should probably be Norm+PCA+KNN, or maybe Norm+PCA+RandomForest. As I discussed w/ Thiago, it's more common in the literature to go with KNN. Should check a few papers on this.
    - Also dawned on me later that RandomForest would have to be absurdly deep to consider a relevant number of principal components. Seems unsustainable to have a model like this w/ 1000+ variables.
    - Maybe a mixture model could be appropriate to this problem if we imagine that each cell would present some characteristics from a number of different cell "archetypes", and that the class of the cell is then determined by having more of one set of characteristics. Each of the three previous "major cell categories" could represent an "archetype".
2. Classification
    - 100% accuracy is either overfit or leak; add CV to check overfit and maybe do a correlation matrix to check leak? Could also get univariate gini to avoid the random 20k x 20k matrix.
    - Gini is a poor choice as this is multinomial... Attempted to go with Adj Mutual Information, but ran too slowly and bust my RAM twice. Settling with Adj Rand Index, but doesn't seem like there are any largely informative variables by themselves.
    - To avoid this, selected only a couple of principal components.
    - Also tried to add a step with the clustering as a feature generation step:
        - For KNN this doesn't really have an effect as the clustering dimension doesn't change the performance much;
        - Tried to fit a Logistic Regression on the data, which does yield nice results, but I wouldn't trust the model in this setting without a bunch of additional analysis;
        - Decided to remove this from the outputs.
    - Settled with a basic KNN version, some cross val + scoring only.
    - One additional thing would be to check if the wrong predictions have any spatial information related to our previous efforts in visualization. Maybe the model gets predictions wrong when they are close to the edges between clusters in our classification model. With time, I'd try to bring this information into the model in a few different ways; maybe by performing a power transform to the predicted class or something like this?

## Work Log for CPI

1. Baseline:
    - Rather tempted to spit out a baseline with a linear regression, but this omits any oscilation, which tends to occur for economic data. Thinking of settling for a decomposition which at least includes some seasonality;
    - Used STL + Exponential Smoothing to predict monthly change;
2. A better model:
    - Here, I'd want to see lagged correlations, maybe even something as simple as R2 for PriceStats w/ CPI;
    - A new model could be to use ARIMA by running a Box-Jenkins procedure to determine coefficients, using PriceStats as an external regressor; the series is clearly a leading indicator of CPI.
    - Maybe even start with a simple model, just including PriceStats as a regressor.
    - Maybe I won't have time to do this.

### Notes

- Tried using Vaex, much slower on a file like this.
