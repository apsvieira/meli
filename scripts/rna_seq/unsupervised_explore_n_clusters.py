import os

import pandas as pd
import numpy as np
from sklearn.cluster import AgglomerativeClustering, KMeans
from tqdm import tqdm

from meli.common import get_base_path
from meli.modeling.clustering import search_number_of_clusters


if __name__ == '__main__':
    PROBLEM = 'rna_seq'
    base_path = get_base_path()
    raw_data_path = os.path.join(base_path, 'data/raw')
    raw_data_fp = os.path.join(raw_data_path, 'matrix_unsupervised.csv')
    result_fp = os.path.join(base_path,
                             f'data/{PROBLEM}/n_clusters_opt.parquet')
    matrix = pd.read_csv(raw_data_fp)

    min_n_clusters = 2
    max_n_clusters = int(2 * np.log(matrix.shape[0]))
    search_space = {
        'agglomerative_clustering': {
            'class': AgglomerativeClustering,
            'parameters': {
                'linkage': 'average',
                'affinity': 'cosine',
            },
        },
        'kmeans': {
            'class': KMeans,
            'parameters': {
                'random_state': 2678416867,
            },
        }
    }

    opt_results = []
    for algorithm, spec in tqdm(search_space.items(),
                                desc='Search over algorithms'):
        r = search_number_of_clusters(
            X=matrix,
            cluster_class=spec['class'],
            min_n_clusters=min_n_clusters,
            max_n_clusters=max_n_clusters,
            cluster_kwargs=spec['parameters'],
        )
        r['algorithm'] = algorithm
        opt_results.append(r)

    results_df = pd.concat(opt_results, axis=0)
    results_df.to_parquet(result_fp)
