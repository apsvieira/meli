import os

import pandas as pd

from meli.modeling.data import deterministic_split


if __name__ == '__main__':
    cell_class = 'cell_class'

    current_fp = os.path.abspath(__file__)
    base_path = os.path.abspath(os.path.join(current_fp, '../../..'))

    raw_data_path = os.path.join(base_path, 'data/raw')
    raw_data_fp = os.path.join(raw_data_path, 'matrix_supervised.csv')
    matrix = pd.read_csv(raw_data_fp, header=None)
    matrix = matrix.rename(columns={0: cell_class})
    matrix = matrix.rename(columns=str)

    split_data_path = os.path.join(base_path, 'data/rna_seq')
    os.makedirs(split_data_path, exist_ok=True)
    develop, evaluation = deterministic_split(matrix, cell_class, 0.3)
    develop.to_parquet(os.path.join(split_data_path, 'develop.parquet'))
    evaluation.to_parquet(os.path.join(split_data_path, 'evaluation.parquet'))
