.PHONY: pull_raw_data push_raw_data pull_data push_data
pull_raw_data:
	gsutil -m rsync -r \
		gs://meli-data-interview-2021/data/raw/ \
		data/raw/

push_raw_data:
	gsutil -m rsync -r \
		data/raw/ \
		gs://meli-data-interview-2021/data/raw/

pull_data:
	gsutil -m rsync -r \
		gs://meli-data-interview-2021/data/ \
		data/

push_data:
	gsutil -m rsync -r \
		data/ \
		gs://meli-data-interview-2021/data/


.PHONY: rna_seq_supervised
rna_seq_supervised:
	python scripts/rna_seq/split_supervised_data.py
