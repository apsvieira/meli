import altair as vg


def configure_title(chart: vg.Chart) -> vg.Chart:
    chart = chart.configure_title(
        fontSize=16,
        font='Roboto',
        anchor='middle',
        color='#222222'
    )
    return chart
