from typing import List

import altair as vg
from pandas import cut, DataFrame, Series

from meli.visualization.common import configure_title


def histogram(x: Series, title: str) -> vg.Chart:
    name = x.name if x.name is not None else 0
    results = x.value_counts()
    results = results.sort_index()
    results = results.reset_index()
    results = results.rename(columns={name: 'Frequency',
                                      'index': 'Category'})
    results['Category'] = results['Category'].astype(str)
    return bar(results, 'Category', 'Frequency', title)


def bar(data: DataFrame, x: str, y: str, title: str) -> vg.Chart:
    chart = vg.Chart(data).mark_bar().encode(
        x=vg.X(x),
        y=vg.Y(y),
        tooltip=[x, y],
    ).properties(
        title=title,
    )
    chart = configure_title(chart)
    return chart
