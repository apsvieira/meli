from typing import List, Optional

import altair as vg
from pandas import DataFrame

from meli.visualization.common import configure_title


def regplot(
    data: DataFrame,
    x: str,
    y: str,
    title: str,
    domain: Optional[List[float]] = None,
) -> vg.Chart:
    if domain is None:
        y_min = data[y].min()
        y_max = data[y].max()
        domain = [0.9 * y_min, 1.1 * y_max]

    scale = vg.Scale(domain=domain)
    base = vg.Chart(data).mark_line().encode(
        x=vg.X(x),
        y=vg.Y(y, scale=scale),
    )

    linear_fit = (
        base
        .transform_regression(x, y,
                              method='linear',
                              order=1,
                              as_=[x, str(1)])
        .mark_line(strokeDash=[2, 5])
        .transform_fold([str(1)], as_=['degree', y])
        .encode(color=vg.value('grey'))
    )
    chart = vg.layer(base, linear_fit)
    chart = chart.properties(width=800, height=600, title=title)
    chart = configure_title(chart)
    return chart
