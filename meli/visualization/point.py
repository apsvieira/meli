from typing import Optional

import altair as vg
from pandas import DataFrame

from meli.visualization.common import configure_title


def scatter(
    data: DataFrame,
    x: str,
    y: str,
    title: str,
    color: Optional[str] = None,
) -> vg.Chart:
    tooltip_cols = [x, y]
    if color is not None:
        tooltip_cols.append(color)

    chart = vg.Chart(data).mark_point().encode(
        x=vg.X(x),
        y=vg.Y(y),
        tooltip=tooltip_cols,
    )
    if color is not None:
        chart = chart.encode(color=vg.Color(f'{color}:N'))
    chart = chart.properties(width=800, height=600, title=title)
    chart = chart.interactive()
    chart = configure_title(chart)
    return chart
