from typing import Dict, List

from numpy import log1p
from pandas import DataFrame, Series
from sklearn import metrics
from sklearn.decomposition import PCA
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import FunctionTransformer, StandardScaler
from tqdm import tqdm


def evaluate_clustering(
    X: DataFrame,
    pred: Series,
    n_clusters: int,
) -> List[Dict[str, float]]:
    clustering_metrics = {
        'Silhouette': metrics.silhouette_score,
        'Davies-Bouldin': metrics.davies_bouldin_score,
        'Calinski-Harabasz': metrics.calinski_harabasz_score,
    }

    results = [{'n_clusters': n_clusters,
                'metric': metric,
                'value': fn(X, pred)}
               for metric, fn in clustering_metrics.items()]
    return results


def make_clustering_pipeline(
    n_components,
    cluster_class,
    n_clusters: int,
    cluster_kwargs: Dict,
) -> Pipeline:
    dim_reduction = make_dimensionality_reduction_pipeline(n_components)
    clustering = Pipeline([
        ('dimensionality_reduction', dim_reduction),
        ('cluster', cluster_class(n_clusters=n_clusters, **cluster_kwargs)),
    ])
    return clustering


def search_number_of_clusters(
    X: DataFrame,
    cluster_class,
    min_n_clusters: int,
    max_n_clusters: int,
    cluster_kwargs: Dict,
) -> DataFrame:
    search_space = range(min_n_clusters, max_n_clusters + 1)
    search_results = []
    for n_clusters in tqdm(search_space, desc='n_clusters'):
        clustering = make_clustering_pipeline(
            0.95, cluster_class, n_clusters, cluster_kwargs)
        pred = clustering.fit_predict(X)
        search_results += evaluate_clustering(X, pred, n_clusters)

    results_table = DataFrame(search_results)
    results_table['parameters'] = str(cluster_kwargs)

    return results_table


def make_dimensionality_reduction_pipeline(n_components) -> Pipeline:
    return Pipeline([
        ('log1p', FunctionTransformer(log1p)),
        ('scaling', StandardScaler(with_mean=True, with_std=False)),
        ('pca', PCA(n_components=n_components, random_state=480022681)),
    ])
