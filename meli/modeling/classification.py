from scipy.spatial.distance import cosine
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline

from meli.modeling.clustering import make_dimensionality_reduction_pipeline


def make_classification_pipeline(n_components, n_neighbors: int) -> Pipeline:
    dimensionality_reduction = make_dimensionality_reduction_pipeline(
        n_components)
    clf = KNeighborsClassifier(n_neighbors=n_neighbors,
                               weights='distance',
                               metric=cosine,
                               n_jobs=-1)
    p = Pipeline([
        ('dimensionality_reduction', dimensionality_reduction),
        ('scaling', StandardScaler(with_mean=True, with_std=True)),
        ('classifier', clf),
    ])
    return p
