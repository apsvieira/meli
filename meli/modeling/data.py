from math import floor
from typing import Tuple

from pandas import DataFrame
from sklearn.model_selection import train_test_split


def deterministic_split(
    X: DataFrame,
    stratify: str,
    test_fraction: float,
) -> Tuple[DataFrame, DataFrame]:
    assert 0 < test_fraction < 1
    test_size = floor(X.shape[0] * test_fraction)
    train, test = train_test_split(
        X,
        stratify=X[stratify],
        test_size=test_size,
        random_state=592406144,
    )
    return train, test
