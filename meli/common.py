import os


def get_base_path() -> str:
    project_home = os.environ.get('_PROJECT_HOME')
    if project_home is None:
        raise ValueError('Set environment variable _PROJECT_HOME'
                         'before executing. See README for details.')

    base_path = os.path.expanduser(project_home)
    return base_path
