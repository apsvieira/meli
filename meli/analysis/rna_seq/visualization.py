from numpy import log1p, sum as np_sum
from pandas import DataFrame
from pandas.io.formats.style import Styler
from sklearn.cluster import AgglomerativeClustering
from sklearn.decomposition import PCA
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import FunctionTransformer, StandardScaler


def make_visualization_pipeline() -> Pipeline:
    visualization = Pipeline([
        ('log1p', FunctionTransformer(log1p)),
        ('scaling', StandardScaler(with_mean=True, with_std=True)),
        ('pca', PCA(n_components=3, random_state=4049621617)),
    ])
    return visualization


def make_default_clustering_pipeline() -> Pipeline:
    return Pipeline([
        ('log1p', FunctionTransformer(log1p)),
        ('scaling', StandardScaler(with_mean=True, with_std=False)),
        ('pca', PCA(n_components=0.95, random_state=480022681)),
        ('cluster', AgglomerativeClustering(n_clusters=3, linkage='average',
                                            affinity='cosine')),
    ])


def report_pca_results(pca: PCA) -> Styler:
    r = dict()
    r['n_components'] = pca.n_components_
    exp_variance = pca.explained_variance_ratio_
    r['explained_variance_top_10'] = np_sum(exp_variance[:10])
    r['explained_variance_top_100'] = np_sum(exp_variance[:100])
    r['components_with_at_least_1pc'] = np_sum(exp_variance >= 0.01)
    r['components_with_at_least_0.1pc'] = np_sum(exp_variance >= 0.001)
    results = DataFrame(data=r, index=['']).T
    view = results.style

    return view
