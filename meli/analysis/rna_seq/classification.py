from matplotlib import pyplot as plt
from pandas import DataFrame, Series
from seaborn import heatmap
from sklearn.pipeline import Pipeline
from sklearn.model_selection import cross_validate, StratifiedKFold
from sklearn.metrics import confusion_matrix, f1_score

from meli.modeling.classification import make_classification_pipeline


def make_baseline() -> Pipeline:
    return make_classification_pipeline(n_components=50, n_neighbors=5)


def report_multivariate_classification_metrics(
    clf: Pipeline,
    X: DataFrame,
    y: Series,
) -> DataFrame:
    cv = StratifiedKFold(5, shuffle=True, random_state=667639913)
    scores = cross_validate(
        clf, X, y, cv=cv, n_jobs=-1,
        scoring=['f1_macro', 'f1_micro'],
    )
    return DataFrame(scores)


def report_classification(y_true, y_pred):
    print(f"""
    Model Performance:

    F1-Score (Micro): {f1_score(y_true, y_pred, average='micro')}
    F1-Score (Macro): {f1_score(y_true, y_pred, average='macro')}
    """)

    confusion = confusion_matrix(y_true, y_pred)
    _, ax = plt.subplots(figsize=(16, 12))
    heatmap(confusion, cmap='Blues', vmin=0,
            annot=True, ax=ax, cbar=False)
