# MELI Interview Process

Code related to MELI interview process for data science positions.

Results generated can be viewed as HTML files in the `docs/report/` folder.
These results can be replicated by executing the notebooks in `notebooks/`.

## Requirements

List of software required to execute the code in this repository.

1. Python 3 (recommend 3.8 or higher, I used 3.8 for development);
2. Poetry (installation guide [here](https://python-poetry.org/docs/#installation));
3. Make (not exactly required, you can copy and past commands from the Makefile as needed);
4. [Google Cloud SDK](https://cloud.google.com/sdk/docs/install);

## Python Environment Setup

This project uses Poetry for environment management.
To set up the poetry environment, simply run `poetry install` on the root folder.

If you'd like a Jupyter Notebook in the environment given, run:
1. `poetry shell` (which will start a new shell session in the environment);
2. `export _PROJECT_HOME="$(pwd)"`
3. `jupyter lab`

## Data

Data is made available via a Google Cloud Storage bucket, from the files given. Please note that maintaining and accessing these files will incur in financial charges to me personally. As such, all files will be deleted 90 days after sharing (i.e. 2021-12-17). If you need to access these files after this date, please download them manually from [Google Drive](https://drive.google.com/drive/folders/1FqVaRf1v9yOAh9AlL-tuyp7RF4CgZF8X).

### Downloading data

To download all artifacts generated, `make pull_data`

## Note on reproducibility

Where applicable, random states are set to integers sampled from `[0, 2^32[` using `numpy.random.randint`. Each random state is set to a separately sampled number.
