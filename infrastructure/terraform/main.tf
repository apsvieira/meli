terraform {
  backend "http" {}
}

provider "google" {
  project = "meli-326418"
  region  = "us-central1"
}

locals {
  name = "meli-data-interview-2021"
}
