output "bucket" {
  value       = google_storage_bucket.meli.url
  description = "Storage bucket for Meli Interview data"
}