resource "google_storage_bucket" "meli" {
  name          = local.name
  location      = "US"
  force_destroy = true

  uniform_bucket_level_access = true
  # Archive after 90 days.
  lifecycle_rule {
    action {
        type = "Delete"
    }
    condition {
        age = 90
    }
  }
}
